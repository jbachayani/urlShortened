package Bean;

import java.io.Serializable;
import java.sql.Timestamp;

public class SettingBean implements Serializable {
    private Timestamp startedAt;
    private Timestamp expiredAt;
    private int downloadLimit;
    private int clickUsed;
    private int id;
    private String tableName = "settings";

    public SettingBean() {}

    public Timestamp getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Timestamp startedAt) {
        this.startedAt = startedAt;
    }

    public Timestamp getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(Timestamp expiredAt) {
        this.expiredAt = expiredAt;
    }

    public int getDownloadLimit() {
        return downloadLimit;
    }

    public void setDownloadLimit(int downloadLimit) {
        this.downloadLimit = downloadLimit;
    }

    public int getClickUsed() {
        return clickUsed;
    }

    public void setClickUsed(int clickUsed) {
        this.clickUsed = clickUsed;
    }

    public String getTableName() {
        return tableName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
