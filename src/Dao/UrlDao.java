package Dao;

import Bean.PasswordBean;
import Bean.SettingBean;
import Bean.UrlBean;
import Core.DatabaseCore;
import Core.DateCore;
import Core.KeyGeneratorCore;
import Core.SessionCore;
import Dao.Abstract.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UrlDao extends Dao<UrlBean, String> {
    private PreparedStatement query;
    private ResultSet result;

    public UrlDao() {}

    /**
     *
     * @param object
     * @param urlShortened
     * @return
     */
    @Override
    public UrlBean find(UrlBean object, String urlShortened) {
        try {
            this.query = DatabaseCore.getInstance().prepareStatement("SELECT url FROM "+ object.getTableName() +" WHERE url_short = ? AND user_id = ?;");
            this.query.setString(1, urlShortened);
            this.query.setInt(2, SessionCore.returnUserId());

            result = this.query.executeQuery();

            if (result.next()) {
                object.setUrl(result.getString("url"));

                return object;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param object
     * @param form
     * @return
     */
    @Override
    public UrlBean create(UrlBean object, Map<String, String> form) {
        String urlShortened = "http://localhost:8090/domain/" + KeyGeneratorCore.generateUniqueKey();

        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("INSERT INTO "+ object.getTableName() +" (url, url_short, created_at, user_id) VALUES (?, ?, ?, ?);");

            this.query.setString(1, form.get("url"));
            this.query.setString(2, urlShortened);
            this.query.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            this.query.setInt(4, SessionCore.returnUserId()); // TODO: METTRE ID USER
            this.query.executeUpdate();

            object.setUrl_short(urlShortened);
            return object;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return object;
    }

    /**
     *
     * @param object
     * @param form
     */
    public void createWithPassword(PasswordBean object, Map<String, String> form) {
        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("INSERT INTO " + object.getTableName() + " (password, active, id_url) VALUES (?, ?, LAST_INSERT_ID());");

            this.query.setString(1, (form.get("password_1")));
            this.query.setInt(2, 1);

            this.query.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param object
     * @param form
     */
    public void createWithPasswords(PasswordBean object, Map<String, String> form) {
        try {
            int urlId = 0;

            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("SELECT MAX(id) AS id FROM urls WHERE url = ?  AND user_id = ?"); // TODO Change id USER

            this.query.setString(1, (form.get("url")));
            this.query.setInt(2, SessionCore.returnUserId());

            result = this.query.executeQuery();

            if (result.next()) {
                urlId = result.getInt("id");
            }

            for(Map.Entry<String, String> entry : form.entrySet()) {
                if (entry.getKey().matches("password_(.*)") && !entry.getValue().isEmpty()) {
                    this.query = DatabaseCore
                            .getInstance()
                            .prepareStatement("INSERT INTO " + object.getTableName() + "(password, active, id_url) VALUES (?, ?, ?);");

                    this.query.setString(1, (entry.getValue()));
                    this.query.setInt(2, 1);
                    this.query.setInt(3, urlId);

                    this.query.executeUpdate();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param object
     * @param urlShorted
     * @return
     */
    public boolean urlHaveDate(UrlBean object, String urlShorted) {
        try {
            this.query = DatabaseCore.getInstance().prepareStatement("SELECT expired_at FROM "+ object.getTableName() + " JOIN settings s ON urls.id = s.id_url WHERE url_short = ?;");

            this.query.setString(1, urlShorted);

            result = this.query.executeQuery();

            return result.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     *
     * @param object
     * @param form
     */
    public void createWithDate(SettingBean object, Map<String, String> form) {
        try {
            int urlId = 0;

            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("SELECT MAX(id) AS id FROM urls WHERE url = ?  AND user_id = ?");

            this.query.setString(1, (form.get("url")));
            this.query.setInt(2, SessionCore.returnUserId());// TODO Change id USER

            result = this.query.executeQuery();

            if (result.next()) {
                urlId = result.getInt("id");
            }

            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("INSERT INTO "+ object.getTableName() +" (started_at, expired_at, download_limit, id_url) VALUES (?, ?, ?, ?);");

            this.query.setTimestamp(1, (Timestamp) DateCore.convertStringToDate(form.get("date-start")));
            this.query.setTimestamp(2, (Timestamp) DateCore.convertStringToDate(form.get("date-end")));

            if ((form.get("limit") != null && !form.get("limit").isEmpty())) {
                this.query.setInt(3, Integer.parseInt(form.get("limit")));
            } else {
                this.query.setInt(3, 0);
            }

            this.query.setInt(4, urlId);

            this.query.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param object
     * @param form
     */
    public void createClickLimit(SettingBean object, Map<String, String> form) {
        try {
            int urlId = 0;

            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("SELECT MAX(id) AS id FROM urls WHERE url = ?  AND user_id = ?");

            this.query.setString(1, (form.get("url")));
            this.query.setInt(2, SessionCore.returnUserId());// TODO Change id USER

            result = this.query.executeQuery();

            if (result.next()) {
                urlId = result.getInt("id");
            }

            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("INSERT INTO "+ object.getTableName() +" (download_limit, id_url) VALUES (?, ?);");

            this.query.setInt(1, Integer.parseInt(form.get("limit")));
            this.query.setInt(2, urlId);

            this.query.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param object
     * @param urlShorted
     * @return
     */
    public boolean urlHavePassword(UrlBean object, String urlShorted) {
        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("SELECT u.id FROM " + object.getTableName() + " as u" +
                            " JOIN passwords p ON u.id = p.id_url" +
                            " WHERE u.url_short = ?");

            this.query.setString(1, urlShorted);

            result = this.query.executeQuery();

            return result.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     *
     * @param object
     * @param urlShorted
     * @return
     */
    public List<String> findUrlWithPasswords(UrlBean object, String urlShorted) {
        List<String> passwords = new ArrayList<>();
        int count = 0;

        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("SELECT p.password FROM " + object.getTableName() + " as u" +
                            " JOIN passwords p ON u.id = p.id_url" +
                            " WHERE u.url_short = ?");

            this.query.setString(1, (urlShorted));

            result = this.query.executeQuery();

            while (result.next()) {
                passwords.add(result.getString("password"));
            }

            return passwords;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new NullPointerException();
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public UrlBean edit(UrlBean object) {
        return null;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public UrlBean update(UrlBean object) {
        return null;
    }

    /**
     *
     * @param object
     */
    @Override
    public void delete(UrlBean object) {}
}