package Dao.Factory;

import Bean.PasswordBean;
import Bean.SettingBean;
import Bean.UrlBean;
import Bean.UserBean;

public class BeanFactory {

    public UrlBean getUrlBean() {
        return new UrlBean();
    }
    public PasswordBean getPasswordBean() {
        return new PasswordBean();
    }
    public UserBean getUserBean() {
        return new UserBean();
    }
    public SettingBean getSettingBean() {
        return new SettingBean();
    }
}