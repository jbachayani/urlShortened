package Dao;

import Bean.SettingBean;
import Core.DatabaseCore;
import Core.SessionCore;
import Dao.Abstract.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;

public class SettingDao extends Dao<SettingBean, String> {
    private PreparedStatement query;
    private ResultSet result;

    /**
     *
     * @param object
     * @param urlShorted
     * @return
     */
    @Override
    public SettingBean find(SettingBean object, String urlShorted) {
        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("SELECT settings.id, started_at, expired_at, download_limit, clickUsed FROM "+ object.getTableName() +" join urls u on settings.id_url = u.id where url_short = ?");

            this.query.setString(1, urlShorted);

            result = this.query.executeQuery();

            if (result.next()) {
                object.setStartedAt(result.getTimestamp("started_at"));
                object.setExpiredAt(result.getTimestamp("expired_at"));
                object.setDownloadLimit(result.getInt("download_limit"));
                object.setId(result.getInt("id"));
                object.setClickUsed(result.getInt("clickUsed"));

                return object;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param object
     * @param form
     * @return
     */
    @Override
    public SettingBean create(SettingBean object, Map<String, String> form) {

        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("INSERT INTO "+ object.getTableName() +" (started_at, expired_at, download_limit, user_id) VALUES (?, ?, ?, ?);");

            this.query.setString(1, form.get("date-start"));
            this.query.setString(2, form.get("date-end"));
            this.query.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            this.query.setInt(4, SessionCore.returnUserId()); // TODO: METTRE ID USER
            this.query.executeUpdate();

            return object;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return object;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public SettingBean edit(SettingBean object) {
        return null;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public SettingBean update(SettingBean object) {
        return null;
    }

    /**
     *
     * @param object
     */
    public void updateClickUsed(SettingBean object) {
        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("UPDATE "+ object.getTableName() + " SET clickUsed = ? + 1, started_at = null, expired_at = null WHERE id = ?");

            this.query.setInt(1, object.getClickUsed());
            this.query.setInt(2, object.getId());

            this.query.executeUpdate();

            } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param object
     * @param urlShorted
     * @return
     */
    public SettingBean haveClick(SettingBean object, String urlShorted) {
        try {
            this.query = DatabaseCore
                    .getInstance()
                    .prepareStatement("SELECT download_limit, clickUsed FROM "+ object.getTableName() +" join urls u on settings.id_url = u.id where url_short = ?");

            this.query.setString(1, urlShorted);

            result = this.query.executeQuery();

            if (result.next()) {
                object.setDownloadLimit(result.getInt("download_limit"));
                object.setClickUsed(result.getInt("clickUsed"));

                return object;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param object
     */
    @Override
    public void delete(SettingBean object) {

    }
}
