package Core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

class PropertiesCore {

    static Properties findPropertieFile() {
        try {
            Properties props = new Properties();
            String folderProperties = "resources/";

            InputStream propFile = PropertiesCore.class.getClassLoader().getResourceAsStream(folderProperties + DatabaseCore.getDatabaseConfigurationFile());

            if (propFile != null) {
                props.load(propFile);

                return props;
            } else {
                throw new FileNotFoundException("Le fichier '" + DatabaseCore.getDatabaseConfigurationFile() + "'n'éxiste pas !");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    static HashMap<String, String> loadProperties(Properties propertie) {
        HashMap<String, String> properties = new HashMap<>();

        for (final String name : propertie.stringPropertyNames()) {
            properties.put(name, propertie.getProperty(name));
        }

        return properties;
    }
}
