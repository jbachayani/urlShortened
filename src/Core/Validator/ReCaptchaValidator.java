package Core.Validator;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public class ReCaptchaValidator {
    private final String WEBSITE_VERIFY = "https://www.google.com/recaptcha/api/siteverify?";
    private final String PUBLIC_KEY = "6LcUxl8UAAAAAOB4QozwBYyO_W8EGeAG22XkaFsz";
    private final String SECRET_KEY = "6LcUxl8UAAAAAF354zhg_1ZcRPg1AB-OJ6yzo7BT";

    public ReCaptchaValidator() {}

    public boolean verify (String captchaResponse) {
        try {
            if (captchaResponse != null) {


                String url = WEBSITE_VERIFY
                        + "secret=" + SECRET_KEY
                        + "&response=" + captchaResponse;

                StringBuilder sb = new StringBuilder();
                InputStream res = new URL(url).openStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(res, Charset.forName("UTF-8")));

                int cp;
                while ((cp = rd.read()) != -1) {
                    sb.append((char) cp);
                }
                String jsonText = sb.toString();
                res.close();

                JSONObject json = new JSONObject(jsonText);

                return json.getBoolean("success");
            }
            return false;
        } catch (Exception e) {
            e.getStackTrace();

            return false;
        }
    }
}
