package Core.Validator;

import java.util.Map;

public class FormValidator {
    private Map<String, String> form;
    private String urlField = "url";
    private String passwordField = "password_1";
    private String passwordField2 = "password_2";
    private String passwordField3 = "password_3";
    private String dateStart = "date-start";
    private String dateEnd = "date-end";
    private String downloadLimit = "downloadLimit";

    private String password = "password";
    private String business = "business";
    private String mail = "mail";

    public FormValidator(Map<String, String> form){
        this.form = form;
    }

    public boolean urlOnly() {
        try {
            return form.get(this.passwordField).isEmpty()
                    && !form.get(this.urlField).isEmpty()
                    && form.get(this.dateStart).isEmpty()
                    && form.get(this.dateEnd).isEmpty();

        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean urlWithPassword() {
        try {
            return !form.get(this.urlField).isEmpty()
                    && !form.get(this.passwordField).isEmpty()
                    && form.get(this.passwordField2).isEmpty()
                    && form.get(this.passwordField3).isEmpty()
                    && form.get(this.dateStart).isEmpty()
                    && form.get(this.dateEnd).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean urlWithPasswords() {
        try {
            return !form.get(this.urlField).isEmpty()
                    && !form.get(this.passwordField).isEmpty()
                    && !form.get(this.passwordField2).isEmpty()
                    && !form.get(this.passwordField3).isEmpty()
                    && form.get(this.dateStart).isEmpty()
                    && form.get(this.dateEnd).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean urlWithPasswords_2() {
        try {
            return !form.get(this.urlField).isEmpty()
                    && !form.get(this.passwordField).isEmpty()
                    && !form.get(this.passwordField2).isEmpty()
                    && form.get(this.dateStart).isEmpty()
                    && form.get(this.dateEnd).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean urlWithPasswords_3() {
        try {
            return !form.get(this.urlField).isEmpty()
                    && !form.get(this.passwordField).isEmpty()
                    && !form.get(this.passwordField3).isEmpty()
                    && form.get(this.dateStart).isEmpty()
                    && form.get(this.dateEnd).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean signUp() {
        try {
            return !form.get(this.password).isEmpty()
                    && !form.get(this.business).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean allFields() {
        try {
            return !form.get(this.passwordField).isEmpty()
                    && !form.get(this.passwordField2).isEmpty()
                    && !form.get(this.passwordField3).isEmpty()
                    && !form.get(this.urlField).isEmpty()
                    && !form.get(this.dateStart).isEmpty()
                    && !form.get(this.dateEnd).isEmpty()
                    && !form.get(this.downloadLimit).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean urlWithPasswordsAndDate() {
        try {
            return !form.get(this.passwordField).isEmpty()
                    && !form.get(this.urlField).isEmpty()
                    && !form.get(this.dateStart).isEmpty()
                    && !form.get(this.dateEnd).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean logIn() {
        try {
            return !form.get(this.mail).isEmpty()
                    && !form.get(this.password).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean urlWithPasswordsAndDate_2() {
        try {
            return !form.get(this.passwordField).isEmpty()
                    && !form.get(this.passwordField2).isEmpty()
                    && !form.get(this.urlField).isEmpty()
                    && !form.get(this.dateStart).isEmpty()
                    && !form.get(this.dateEnd).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }


    public boolean urlWithPasswordsAndDate_3() {
        try {
            return !form.get(this.passwordField).isEmpty()
                    && !form.get(this.passwordField2).isEmpty()
                    && !form.get(this.passwordField3).isEmpty()
                    && !form.get(this.urlField).isEmpty()
                    && !form.get(this.dateStart).isEmpty()
                    && !form.get(this.dateEnd).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

    public boolean urlWithDate() {
        try {
            return !form.get(this.urlField).isEmpty()
                    && !form.get(this.dateStart).isEmpty()
                    && !form.get(this.dateEnd).isEmpty()
                    && form.get(this.passwordField).isEmpty()
                    && form.get(this.passwordField2).isEmpty()
                    && form.get(this.passwordField3).isEmpty();
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }

}
