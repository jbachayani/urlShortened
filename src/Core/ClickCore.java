package Core;

import Bean.SettingBean;

public class ClickCore {

    public static boolean isValid (SettingBean object) {
        if (object.getClickUsed() >= object.getDownloadLimit()) {
            return false;
        } else if (object.getDownloadLimit() == 0) {
            return true;
        }

        return true;
    }
}
