package Core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Properties;

public final class DatabaseCore {
    private static final String DATABASE_CONFIGURATION_FILE = "database.properties";
    private static Connection connection = null;

    public static Connection getInstance() {
        DatabaseCore.loadMysqlDriver();

        Properties propertieFile = PropertiesCore.findPropertieFile();
        HashMap<String, String> propertiesList = PropertiesCore.loadProperties(propertieFile);

        String db_name = propertiesList.get("db_name");
        String db_host = propertiesList.get("db_host");
        String db_port = propertiesList.get("db_port");
        String db_username = propertiesList.get("db_username");
        String db_password= propertiesList.get("db_password");

        if (connection == null) {
            try {
                String url = "jdbc:mysql://"+ db_host +":"+ db_port +"/"+ db_name;

                connection = DriverManager.getConnection(url, db_username, db_password);

                System.out.println("La base de donnée est connecté");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    private static void loadMysqlDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static String getDatabaseConfigurationFile() {
        return DATABASE_CONFIGURATION_FILE;
    }
}
