package Core;

import java.util.UUID;

public class KeyGeneratorCore {
    public static String generateUniqueKey() {
        return UUID.randomUUID().toString().substring(1,8);
    }
}
