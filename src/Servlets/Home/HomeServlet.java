package Servlets.Home;

import Bean.UrlBean;
import Core.DateCore;
import Core.SessionCore;
import Core.Validator.FormValidator;
import Core.Validator.ReCaptchaValidator;
import Core.Validator.UrlValidator;
import Dao.Factory.BeanFactory;
import Dao.Factory.DaoFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "homeServlet")
public class HomeServlet extends HttpServlet {
    private final String VIEW = "/views/index.jsp";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> formParameters = new HashMap<>();

        BeanFactory bean = new BeanFactory();
        DaoFactory dao = new DaoFactory();

        HttpSession session = request.getSession();
        new SessionCore(request, session);
        boolean success = false;

        String urlParameter = request.getParameter("url");
        String passwordParameter = request.getParameter("password_1");
        String passwordParameter2 = request.getParameter("password_2");
        String passwordParameter3 = request.getParameter("password_3");
        String captchaParameter = request.getParameter("g-recaptcha-response");
        String startAt = request.getParameter("date-start");
        String endAt = request.getParameter("date-end");
        String downloadLimit = request.getParameter("limit");

        formParameters.put("url", urlParameter);
        formParameters.put("password_1", passwordParameter);
        formParameters.put("password_2", passwordParameter2);
        formParameters.put("password_3", passwordParameter3);
        formParameters.put("recaptcha", captchaParameter);
        formParameters.put("date-start", startAt);
        formParameters.put("date-end", endAt);
        formParameters.put("limit", downloadLimit);

        FormValidator formValidator = new FormValidator(formParameters);
        ReCaptchaValidator captchaValidator = new ReCaptchaValidator();

        if (captchaValidator.verify(captchaParameter)) {
            // On check si juste l'url est renseignée
            if (formValidator.urlOnly()) {
                if (UrlValidator.isValid(urlParameter)) {
                    UrlBean urlShortened = dao.getUrlDao().create(bean.getUrlBean(), formParameters);

                    if (downloadLimit != null && !downloadLimit.isEmpty()) {
                        dao.getUrlDao().createClickLimit(bean.getSettingBean(), formParameters);
                    }
                    request.setAttribute("msg", "Votre url : ");
                    request.setAttribute("url", urlShortened.getUrl_short());
                    success = true;

                    this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                } else {
                    request.setAttribute("error", "L'url n'est pas valide !");

                    this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                }
            }
            // On vérifie la présence des passwords
            if (formValidator.urlWithPassword() || formValidator.urlWithPasswords() || formValidator.urlWithPasswords_2() || formValidator.urlWithPasswords_3()) {
                if (UrlValidator.isValid(urlParameter)) {
                    UrlBean urlShortened = dao.getUrlDao().create(bean.getUrlBean(), formParameters);

                    dao.getUrlDao().createWithPasswords(bean.getPasswordBean(), formParameters);

                    if (!downloadLimit.isEmpty()) {
                        dao.getUrlDao().createClickLimit(bean.getSettingBean(), formParameters);
                    }

                    request.setAttribute("msg", "Votre url : ");
                    request.setAttribute("url", urlShortened.getUrl_short());
                    success = true;

                    this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                } else {
                    request.setAttribute("error", "L'url n'est pas valide !");

                    this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                }
            }

            // On regarde s'il il y a la date et url
            if (formValidator.urlWithDate()) {
                if (UrlValidator.isValid(urlParameter)) {
                    if (DateCore.checkDates(startAt, endAt)) {
                        UrlBean urlShortened = dao.getUrlDao().create(bean.getUrlBean(), formParameters);
                        dao.getUrlDao().createWithDate(bean.getSettingBean(), formParameters);

                        request.setAttribute("msg", "Votre url : ");
                        request.setAttribute("url", urlShortened.getUrl_short());
                        success = true;

                        this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                    } else {
                        request.setAttribute("error", "Les dates ne sont pas valides !");

                        this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                    }
                }
            }
            // On vérifie la présence des mdp, url et date
            if (formValidator.urlWithPasswordsAndDate() || formValidator.urlWithPasswordsAndDate_2() || formValidator.urlWithPasswordsAndDate_3()) {
                if (UrlValidator.isValid(urlParameter)) {
                    if (DateCore.checkDates(startAt, endAt)) {
                        UrlBean urlShortened = dao.getUrlDao().create(bean.getUrlBean(), formParameters);
                        dao.getUrlDao().createWithPasswords(bean.getPasswordBean(), formParameters);
                        dao.getUrlDao().createWithDate(bean.getSettingBean(), formParameters);

                        request.setAttribute("msg", "Votre url : ");
                        request.setAttribute("url", urlShortened.getUrl_short());
                        success = true;

                        this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                    } else {
                        request.setAttribute("error", "Les dates ne sont pas valides !");

                        this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
                    }
                }
            }
        } else {
            request.setAttribute("error", "Captha obligatoire !");

            success = true;

            this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);
        }

        if (!success) {
            this.getServletContext().getRequestDispatcher(VIEW).forward(request, response);

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(this.VIEW).forward(request, response);
    }
}