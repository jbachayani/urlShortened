package Servlets.Account;

import Core.SessionCore;
import Core.Validator.FormValidator;
import Core.Validator.MailValidator;
import Dao.Factory.DaoFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Account", urlPatterns = "/account")
public class AccountServlet extends HttpServlet {
    private final String HOME_VIEW = "/views/index.jsp";
    private final String LOGIN_VIEW = "/views/users/login.jsp";
    private final String ACCOUNT_VIEW = "/views/account/account.jsp";

    protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();

        Map<String, String> formParameters = new HashMap<>();

        DaoFactory dao = new DaoFactory();

        HttpSession session = request.getSession();

        new SessionCore(request, session);

        String mail = request.getParameter("mail");
        String passwordParameter = request.getParameter("password");
        String business = request.getParameter("business");

        formParameters.put("mail", mail);
        formParameters.put("password", passwordParameter);
        formParameters.put("business", business);

        FormValidator formValidator = new FormValidator(formParameters);


        if (dao.getUserDao().update(formParameters)) {
            this.getServletContext().getRequestDispatcher(ACCOUNT_VIEW).forward(request, response);
        } else {
            request.setAttribute("error", "Une erreur est survenue !");

            this.getServletContext().getRequestDispatcher(ACCOUNT_VIEW).forward(request, response);
        }

    }

    protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        DaoFactory dao = new DaoFactory();
        HttpSession session = request.getSession();

        new SessionCore(request, session);


       /* System.out.println(dao.getUserDao().findById(SessionCore.returnUserId()).get(1));
        System.out.println(dao.getUserDao().findById(SessionCore.returnUserId()).get(2));
        System.out.println(dao.getUserDao().findById(SessionCore.returnUserId()).get(6));*/

        if (SessionCore.returnUserId() != 1) {
            request.getRequestDispatcher(this.ACCOUNT_VIEW).forward(request, response);
        } else {
            request.getRequestDispatcher(this.LOGIN_VIEW).forward(request, response);
        }

    }
}
